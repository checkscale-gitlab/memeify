FROM python:3.6

WORKDIR /usr/src/app
COPY *.py ./
RUN pip install discord.py coloredlogs python-dotenv

CMD ["python", "memeify.py"]